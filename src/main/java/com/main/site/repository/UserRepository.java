package com.main.site.repository;

import com.main.site.entity.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
  @Query(value = "SELECT p FROM User p WHERE p.userEmail = :user_email")
  User getUserByEmail(@Param("user_email")String userEmail);

  @Query(value = "INSERT INTO users (user_email, user_password_hash) VALUES (:user_email, :user_password_hash)", nativeQuery=true)
  User saveUser(@Param("user_email")String userEmail, @Param("user_password_hash")String userPasswordHash);

  @Query(value = "SELECT p FROM User p WHERE p.userLogin = :user_login")
  User getUserByLogin(@Param("user_login")String userLogin);
}