package com.main.site.controllers;

import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.slf4j.LoggerFactory;

@Controller
public class AbstractController {
    Logger logger = LoggerFactory.getLogger(this.getClass());
}
